const moongose = require("mongoose");

const libroSchema = moongose.Schema(
  {
    nombre: String,
    nombre_autor: String,
    precio: Number,
    disponible: Boolean
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

const LibroModel = moongose.model("libros", libroSchema)

module.exports = LibroModel