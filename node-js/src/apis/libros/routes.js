const { Router } = require("express");

const {
  mostrar,
  crear,
  actualizar,
  mostrarPorId,
  eliminar,
} = require("./controller");

const libros = Router();

libros.get("/libros", mostrar);
libros.get("/libros/:libroId", mostrarPorId);
libros.post("/libros", crear);
libros.put("/libros/:libroId", actualizar);
libros.delete("/libros/:libroId", eliminar);

module.exports = libros;
