const LibroModel = require("./models/Libro");

const mostrar = async (req, res) => {
  await LibroModel.find()
    .then((libros) => {
      if (libros.length === 0) {
        return res.json({
          message: "No hay registros",
        });
      }
      return res.json(libros);
    })
    .catch((error) => {
      console.log(error);
      return res.json(error);
    });
};

const mostrarPorId = async (req, res) => {
  const { libroId } = req.params;
  await LibroModel.findById(libroId)
    .then((libro) => {
      return res.json(libro);
    })
    .catch((err) => {
      console.log(err);
      return res.json({ message: "No se encontró el libro" });
    });
};

const crear = async (req, res) => {
  const { nombre, nombre_autor, precio, disponible } = req.body;
  const libro = new LibroModel({ nombre, nombre_autor, precio, disponible });

  await LibroModel.create(libro)
    .then(() => {
      return res.json({
        message: "Libro creado!",
      });
    })
    .catch((err) => {
      console.log(err);
      return res.json({
        err,
      });
    });
};

const actualizar = async (req, res) => {
  const { libroId } = req.params;
  const libro = req.body;
  await LibroModel.findByIdAndUpdate(libroId, libro, {
    new: true,
  })
    .then((libroActualizado) => {
      return res.json({
        message: "Libro actualizado",
        libro: libroActualizado,
      });
    })
    .catch((err) => {
      console.log(err);
      return res.json({
        message: "Libro no encontrado!",
      });
    });
};

const eliminar = async (req, res) => {
  const { libroId } = req.params;
  await LibroModel.findByIdAndDelete(libroId)
    .then((libro) => {
      if (libro === null) {
        return res.json({
          message: "No se encontró el libro",
        });
      }
      return res.json({
        message: "Libro eliminado correctamente",
      });
    })
    .catch((err) => {
      console.log(err);
      return res.json({
        message: "No se pudo eliminar el libro",
      });
    });
};

module.exports = {
  mostrar,
  crear,
  actualizar,
  mostrarPorId,
  eliminar,
};
