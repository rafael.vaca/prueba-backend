const mostrar = (req, res) => {
    return res.json({
        mensaje: "Hola mundo"
    })
}

const mostrarPorId = (req, res) => {
    const { id } = req.params
    return res.json({
        mensaje: `Hola mundo desde mostrar por id: ${id}`
    })
}

const mostrarPorIdDesdeBody = (req, res) => {
    const { id2 } = req.params
    const { id } = req.body
    return res.status(200).json({
        id,
        id2
    })
}

module.exports = {
    mostrar,
    mostrarPorId,
    mostrarPorIdDesdeBody
}