const { Router } = require('express')

const { mostrar, mostrarPorId, mostrarPorIdDesdeBody } = require('./controller')

const prueba = Router()

prueba.get('/prueba', mostrar)
prueba.get('/prueba/:id', mostrarPorId)
prueba.post('/prueba2/:id2', mostrarPorIdDesdeBody)

module.exports = prueba